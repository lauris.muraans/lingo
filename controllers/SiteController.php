<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\forms\LingoGame;

class SiteController extends Controller
{
    public function actionIndex() {
        $lingoGame = new LingoGame();
                
        if (Yii::$app->request->isPost) {
            $lingoGame = Yii::$app->session->get('LingoGame') ?: $lingoGame;
            
            if ($lingoGame->load(Yii::$app->request->post()) && $lingoGame->validateInput()) {
                $lingoGame->guess();
            }
        }
        
        if (!$lingoGame->current_word) {
            $lingoGame->resetGame();
        }
            
        return $this->render('index', [
            'lingoGame' => $lingoGame,
        ]);
    }  

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
}


