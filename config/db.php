<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=lingo',
    'username' => 'user',
    'password' => 'password',
    'charset' => 'utf8',
];
